<img src="./images/under-construction.png" width=128 alt="Under construction...">

# My sketch book, always under construction
Um rascunho de coisas que acho que aprendi, consulte se precisar por sua conta e risco. Por isso não esqueça de confirir sempre a documentação oficial, ela certamente é mais confiável.  

E se algo puder ser melhorado, [me ajude a corrigir](mailto:diogoscs@gmail.com) ;-)

- Git
- [Docker](docker/Docker.md)
- Linux
- Python

