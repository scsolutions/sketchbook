Kubernetes

# Kubernetes

## Sumário

<!-- TOC -->

<!-- TOC -->

## Kubernetes
Orquestrador de containers

Master (por default não rodam containers) e worker (executam as aplicações)


Instalar Kubernetes

**Componentes**
Somente o node com API escreve no 00etc.d (master)
kube schedule, fica no master

**Pod** é a menor unidade do Kubernetes, semelhante a um container no Docker Swarm. Um pod pode armazenar um ou mais containers, ele compartilha o mesmo namespace e por isso só possui um endereço de ip, nome, etc. Podem compartilhar arquivos se estiverem em um ponto de montagem no volume.

**Controllers**
Controla os recuros e informa para o API

## MiniKube
Ambiente do Kubernetes para desenvolvimento local de aplicações, não é recomendável em ambiente de produção. O Minikube utiliza a virtualização, para instalar uma VM que será o host que simulará o ambiente de Kubernetes.

### Instalação

1. Para instalar o MiniKube, é necessário veririficar se o computador suporta virtualização. Em caso negativo, a instalação do VirtualBox e o HyperV por exemplo, habilitam esse recurso.

2. Baixar/instalar o kubectl
    > kubectl controla o gerenciamento de cluster Kubernetes. É possível com eles dar comandos básicos como listar nodes e recursos.

3. Instalar o Minikube
**Comandos MiniKube**
```
# Iniciar o MiniKube
$ minikube start

# Parar o Minikube (VM)
$ minikube stop

# Excluir o MiniKube (VM)
$ minikube delete

# Autenticação no host MiniKube via SSH
$ minikube ssh

# Ip do host
$ minikube ip

# Dashboard com informações do cluster
$ minikube dashboard

# Logs do MiniKube
$ minikube logs
```

## Kubernetes

### Instalação
1. Ler os modulos
2. Instalar o Docker
3. Alterar o cgroup de `cgroupfs` para `systemd`
4. Instalar kubelete, kubeadm e kubectl
5. Faça o download das imagens (opcional), com o comando `kubeadm config images pull`
6. Iniciar o cluster
   > Para iniciar o cluster, basta utilizar o comando `kubeadm init`.
7. Rodar os comandos apresentados na inicialização do cluster para a criação da estrutura de diretórios
8. Fazer o download do pod network
   > Pod network, permite a comunicação entre pods de nodes diferentes. Weave net recomendado.
9. Inserir os nodes no cluster com o comando apresentado
   > Para exibir novamente o comando para adicionar novos nodes, executar o comando `kubeadm token create --print-join command`.

Master, vai ter a função de gerencia do cluster, conversar com o etcd (armazenamento de informações), cuidar da saude dos nodes. Por default não recebe containers

> kubelete: Agente de comunicação do cluster
> kubeadm: Cria o cluster, recomendado devido a sua grande compatibilidade com outras plataformas
> kubectl: Administra o cluster

**kubectl get**    
O subcomando `get`, exibe informações e lista os argumentos
```
$ kubectl get <argumento>

# Exibe nodes
$ kubectl get nodes

# Exibe os nomespaces
$ kubectl get namespaces

# Exibe os pods, você pode especificar um deles ou passar a informação para todos
$ kubectl get pods --all-namespaces
#
$ kubectl get pods -n kube-system

# Usando o parametro `-o wide` exibe onde os pods e containeres estão rodando
$ kubectl get pods -n kube-system -o wide

# TESTAR
# 
$ kubectl get pods nginx -o wide

# Exibe o pod em modo yaml, auxilia na criação de um pod em modo yaml
$ kubectl get pods -o yaml

# Exibe os serviç os
$ kubectl get service

# Exibe os endpoints do service
$ kubectl get endpoints
```
> Para exibir tudo que foi criado, basta utilizar os comandos `kubectl get all` ou `kubectl get pods,services,endpoints`.

> O autocomplete dos comandos pode ser ativado utilizando `echo "source <kubectl completion bash)" >> /root/.bashrc`

**kubectl describe**    
O subcomando `describe` exibe detalhes do argumento.
```
$ kubectl describe <argumento>

# Exibe detalhes desde a criação do node como configurações e eventos
$ kubectl describe nodes

# Exibe detalhes desde a criação do node como configurações e eventos
$ kubectl describe node <nomedonode>

# Exibe detalhes dos pods como configurações e eventos
$ kubectl describe pods -n kube-system

# Exibe detalhes dos serviços como IP, Port, labels, etc. 
$ kubectl describe services <nome>
```

**Taints**, define restrições e regras para os nodes, como por exemplo, permitir que o master rode apenas pods internos ou restringe a execução de novos pods e containers.
No exemplo abaixo o taint está restringindo a criação de novos pods no node, caso haja um scale, ele não alocará mais recursos.
```
$ kubectl taint node <nomedonode> key1=value1:NoSchedule

# Remove a linha do taint
$ kubectl taint node <nomedonode> key1:NoSchedule-

# Não executa pods, caso haja algum pode ativo, ele move para outro nó. Muito 
# utilizado em caso de manutenção do nó.
$ kubectl taint node <nomedonode> key1=value1:NoExecute
 ```


> Dica: O comando `kubectl completion bash > /etc/bash_completion.d/kubectl` faz com que os comandos do kubernetes sejam autocompletados pelo bash. Para que o comando funcioone, é necessário que o pacote `bash-completion` esteja instalado.

**kubectl explain**   
Espécie de manpage dos comandos
```
$ kubectl explain services

$ kubectl explain pods
```

**Criação de elementos**
```
# Cria um pod simples. Comando depreciado
$ kubectl run nginx --image=nginx

# Cria um namespace
$ kubectl create namespace <nomedoname>

# Simula uma criação de um pod
$ kubectl run nginx --image=nginx --dry-run=client

# Simula a criação e coloca a saida como yaml redirecionando para um arquivo do
# mesmo tipo
$ kubectl run nginx --image=nginx --dry-run=client -o yaml > arquivo.yaml

# Simula a criação do deployment
$ kubectl create deployment <nomedodeployment> --image=nginx --dry-run=client -o yaml > arquivo.yaml

# Criando um pod ou deployment a partir de um yaml q passa as configurações do 
# pod. Usando o `-n` é possivel setar o namespace do elemento.
$ kubectl create -f arquivo.yaml -n <nomedonamespace>
```
> É possível redirecionar a saida padrão para um yaml, utilizando outros comandos, como por ex: ``kubectl get pods nginx -o yaml``.

**Apagar um pod ou serviço**
```
$ kubectl delete -f arquivo.yaml
# ou
$ kubectl delete pods <nomedopod>

# Apagar um serviço
$ kubectl delete services <nomedoserviço>
```

**Criando e um Service e acessando um pod**   
Com o comando *expose* você expõe um pod publicamente. O expose serve também para criar Services para deployments, pods, etc.
```
$ kubectl expose pod <nomedopod> --port 80

$ kubectl expose pod nginx --port 80

# O Deploy pode ser criado como Cluster, NodePort ou LoadBalancer
$ kubectl expose deployment nginx --type=NodePort --port 80
```
> O Session Affinity cria uma afinidade para o cliente na sessão, isto é, ele tenta manter a conexão do cliente com o pod estabelecida.

> Caso o pod tenha sido criado através de um arquivo yaml, para acessa-lo, é necessário definir uma sessão de exposição da porta, como no seguinte exemplo:
> ```
> containers: 
>   ports:
>     - containerPort: 80
> ```
> O expose no recurso pode ser feito também utilizando o argumento ``--port <numerodaporta>`` na criação.

Services do tipo **ClusterIP**, só podem ser acessados internamente. Os do tipo **NodePort**, podem ser acessados externamente.

>Diagrama do Kubernetes
>Services [Deployment [Replicaset [Pods]]]

Um cluster **Multi Master**, estabelece uma alta disponibilide pro cluster, adicionando nodes master adicionais ao ambiente. É possível que o multi master seja com ETCD (junto com o control plane no mesmo node) e sem ETCD (separado o control plane no mesmo node)

**Escalar réplicas**
```
# Sobe um pod simples
$ kubectl run nginx --image nginx --port=80

$ kubectl scale --replicas=2 deployment nginx
```

**Editar Services e Deployments (quente)**
```
$ kubectl edit service nginx

$ kubectl edit  deployment nginx
```

**Atualizar um deploy ou service a partir do arquivo**
```
$ kubectl replace -f deploy.yml


# Aplica as alterações do arquivo ou cria o recurso.
$ kubectl apply -f arquivo.yml
```

**Entrar/executar comandos no Pod**
```
$ kubectl exec -ti <nomedopod> -- bash
```

O **Limit range** define restrições de recursos como CPU e memória nos namespaces. Todos os pods anexados ao namespace com limit range são afetados. 
No exemplo abaixo, a quantidade máxima de CPU e memória é representada no campo ***default*** e o mínimo garantido no campo ***defaultRequest***.
> É possível configurar limites diretamente nos deployments, através da seção **resources/limits** no arquivo yaml.
```
apiVersion: v1
kind: LimitRange
metadata:
  name: limitando-recursos
spec:
  limits:
  - default:
      cpu: 1
      memory: 100Mi
    defaultRequest:
      cpu: 0.5
      memory: 80Mi
    type: Container
```

O comando `cordon`, coloca o node em modo manutenção, similar ao NoSchedule.
```
$ kubectl cordon <nodename>

$ kubectl uncordon <nodename>
```

Os **Labels** são um conjunto chave valor, que servem para diferenciar e referenciar recursos (nodes, pods, deployments, etc).
```
# Aplicar um label
$  kubectl label nodes <nomedonode> disk=SSD

$# Substituiu a label
$  kubectl label nodes <nomedonode> disk=SSD --overwrite

# Filtrar pods por labels
$ kubectl get pods -l dc=UK

# Listar todas as labels de um recurso

kubectl label pod <nome pod> --list

# Exibe o label como uma coluna
$ kubectl get pods -L dc
```

O **nodeSelector** em arquivos, auxilia na criação de recursos, de mesma label, exemplo:

Os deployments que tiverem o label abaixo, utilizarão nodes de mesma label.
```
nodeSelector:
   disk: SSD
```


> O **Replicaset** cria replicas dos pods nos nodes mais livres, o **Daemonset** **garante** que cada nó tenha ao menos uma replica.

Atualiza uma imagem de um pod, replicaset, deploy, etc.
```
$ kubectl set image daemonsets <nomedodaemon> nginx=nginx:1.15.0

$ kubectl set image pod -n <namespace> <nomedopod> nginx=nginx:1.15.0
```

> O **MaxSurge** é a quantidade de pods a mais que podem ser criados em uma estratégia de update e o **maxUnavailable** é a quantidade de pods que podem ser interrompidos.

O **Rollout** lista as revisões dos deploys
```
# Lista todas as revisões do rollout
$ kubectl rollout history daemonset <nomedademon>

# Exibe detalhes de uma revisão específica.
$ kubectl rollout history daemonset <nomedademon> --revision=2

# Faz um rollback para uma revisão. Para que funcione corretamente, é 
# necessário habilitar um updateStrategy/RollingUpdate.
$ kubectl rollout undo daemonset <nomedadaemon> --to-revision=1

# Exibe o status do rollout em tempo real
$ kubectl rollout status deployment <nomedodeployment>
```

> O **livenessProbe**, verifica a saude do container, se ele está healthy (vivo), caso não esteja, ele reboota o container e o **readnessProbe**, verifica quando o container está pronto ou disponivel para receber requisições.


O `kubectl example` cria arquivos de exemplo para subir recursos como pods e deployments
```
kubectl example pods > pods.yml
```

> Para criar um pod estatico, voce precisa adicionar o manifesto de criação do pod desejado, dentro do diretorio /etc/kubernetes/manifest

Obter o CIDR do cluster
```
kubectl get node -o jsonpath="{range .items[*]}{.metadata.name} {.spec.PodCIDR}"

kubectl describe nodes <nomedonode> | grep PodCIDR
```
Obter o DNS do cluster
```
kubectl get pods -n kube-system
```

> **Init Container**, são containers que prepara os containers para os pods

### Volumes

O volume **empty-dir** não é persistente, caso esteja em um pod, todos os containers dentro dele o compartilham. Sempre é iniciado vazio, pois ele sobe junto com o container. O **persistent-volume ou PV** grava persistentemente os arquivos em disco. O **persistent volume claim ou PVC** consomem recursos de um **PV** que seria uma espécie de alocação do recurso.

**ReadWriteMany**, todos os nós podem montar o volume como read e write. **ReadWriteOnce**, apenas um nó monta o volume como read e write. **ReaOnlyMany**, o volume pode ser montado por vários nós como read.

> **maxUnavailable** é a quantidade máxima de pods que podem ficar indisponíveis durante um processo de rollout update, por exemplo, se estiver configurado para 8, estes serão removidos durante a transição.

**Questão 1**
Criar um pod utilizando a imagem do Nginx 1.18.0, com o nome giropops no namespace strigus.

**Questão 2**
Aumentar a quantidade de replicas do deployment girus, no namespace girus, que está utilizando a imagem do nginx 1.18.0, para 3 replicas. O deployment está no namespace strigus.

**Questão 3**
Precisamos atualizar a versão NGINX do POD giropops. Ele está na versão 1.18.0 e precisamos atualizar para a versão 1.21.1

**Questão 4**
Ingress

**Questão 5**
Day 6 (EKS)