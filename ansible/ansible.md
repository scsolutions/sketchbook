# Ansible

Produzido pela Red Hat, software de automação de processos e provisionamento de infraestrutura. Suas receitas ou playbooks são criadas a partir de arquivos yaml, são um conjunto de instruções para a execução de tarefas nos hosts remotos que podem ser máquinas Linux, macOS, Windows e até elemento de redes como switches e roteadores.

> O Ansible usa o SSH para a comunicação entre o controller (manager) e os hosts gerenciados e como pré requisito de instalação o pyhton.

## Instalação e comunicação com os hosts
1. Instalar o Ansible com o pacote `ansible`. Mais informações (aqui)[https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html] (apenas no servidor que terá as receitas).
2. Gerar uma chave SSH.
   ```
   $ ssh-keygen
   ```
3. Adicionar o conteúdo da chave pública ``id_rsa.pub`` do manager, no arquivo ``authorized_keys`` dos hosts para que haja uma relação de confiança entre elas.

## Inventario de hosts
Agrupamento de hosts para o deploy das configurações, um exemplo da sintaxe pode ser encontrado em ``/etc/ansible/hosts``. Ex do arquivo:
```
# Não pode haver espaço no nome do grupo
[webserver]
10.0.65.201
10.0.72.96

# Pode haver hosts duplicados
[database]
10.0.65.201
```
> O arquivo de hosts deve ser especificado no arquivo de configuração ou através de linha de comando `ansible -i <pathdoinventario>`

**Listar os hosts**
```
# Todos
$ ansible -i <pathdoinventario> --list-hosts all

# Filtrar por grupo
$ ansible -i <pathdoinventario> --list-hosts database
```

## Módulos
Os módulos são açoes ou processos pré-definidos, para a execução de tarefas. Podem ser passados em um comando, atrvés do parâmetro `-m`. Uma lista de módulos pode ser encontrada (aqui)[https://docs.ansible.com/ansible/2.9/modules/list_of_all_modules.html]. Quando um parâmetro é setado, é necessário verificar a necessidade de utilizar argumentos, utilizando o parâmetro `-a`
```
# Executar um ping através de seu módulo em todos os hosts
$ ansible -i /etc/ansible/inventory all -m ping

# Executar um comando específico com o modulo `command`, por exemplo rodar um 
# fix.
$ ansible -i /etc/ansible/inventory db -m command "cat /etc/hostname"

# Como o command é o modo padrão, não é preciso especifica-lo. Basta usar o 
# parametro `-a` e o comando que deseja executar.
$ ansible -i /etc/ansible/inventory all -a "cat /etc/hostname"

# O parametro `-b` torna o usuário root (become=yes)
$ ansible -i /etc/ansible/inventory webservers -m apt -a "name=cmatrix state=present" -b
```

**Módulos interessantes**
- ping: Executa um ping nos hosts
- systemd: Habilita serviços, reinicia, desabilita, etc.
- apt: Instala pacotes
- copy: Copia arquivos
- setup: Pega informações e variáveis (facts) do host remoto, como ip, fqdn, etc.

## Playbooks
São arquivos em forma de receitas passo a passo, com as tarefas e comandos que o Ansible irá executar. Os arquivos são gerados em .yaml e lidos de forma sequencial.

**Executando um playbook**
```
$ ansible-playbook -i <inventoryfile> <playbookfile>

# Ex:
$ ansible-playbook -i hosts main.yml
```

```
# Se no playbook forem passadas tags, é possível dar um bypass nelas ou 
# seleciona-las para serem isoladamente executadas
# EX:
$ cat main.yml
- hosts: k8s-master
  become: yes
  user: ubuntu
  roles:
  - { role: deploy_app_v2, tags: ["deploy_app_v2_role"]  }

# Dando um bypass
$ ansible-playbook -i /etc/ansible/inventory get_hostname.yml --skip-tags "deploy_app_v2_role"
# Executando apenas a role especificada na tag
$ ansible-playbook -i /etc/ansible/inventory get_hostname.yml --tags "deploy_app_v2_role"
```

**Exemplo de playbook**
```
- hosts: webserver # Grupo de hosts que aquela task será executada
  remote_user: ubuntu # Usuario que executará as tasks remotamente
  tasks: # Especifica a task
    - name: Coleta o hostname # O nome da task
      command: hostname # O comando que sera executado
    
    - name: Install apache2  
      become: yes # Eleva para privilégios de root, util quando o comando só pode ser executado como root
      apt: # Modulo que será rodado
        name: apache2 # Nome do pacote
        state: present # Instala o pacote
        update_cache: yes # Parametro do modulo
    
    - name: Habilitar o Apache2
      service:
        name: apache2
        status: started
        enabled: yes
    
    - name: Copia website para a pasta do Apache
      become: yes
      copy:
        src: /home/ubuntu/website
        dest: /var/www/html/
        owner: root
        group: root
        mode: '0644'
      notify: Reiniciando o apache2 # Chamando um handler
  
  handlers: # O handlers serve para executar tarefas repetitivas, são chamadas 
  # dentro das tasks através da sessão `notify`
  - name: Reiniciando o apache2
    service:
      name: nginx
      state: restarted 
```
> Se quiser instalar mais de um pacote, incluir os pacotes em forma de lista na sessão *name*, ex: ´name: ['apache2', 'php']´

## Roles
A role é utilizada quando você quer montar uma estrutura mais complexa de distribuição de infraestrutura ou uma stack mais completa e organizada. como no exemplo a seguir:

**Criando uma estrutura para uma role**
```
$ ansible-galaxy init <role>

$ ansible-galaxy init nginx
$ ansible-galaxy init php
$ ansible-galaxy init mysql
```
Quando você inicia uma role, ela gera uma estrutura de diretorios, 

```

```

> TODO Handler
> TODO Task
> TODO Register
> TODO vars
> TODO SSH ADD
> host_key_checking

## AWX
O AWX é uma aplicação web para gerenciamento de playbooks, tasks, inventarios e jobs do Ansible.

## Ansible Valut
Utilitário para criptografar arquivos com dados sensíveis para o Ansible.

**Comandos**
```
# Criar um arquivo do zero
$ ansible-vault create arquiv

# Criptografar o arquivo (digitar uma senha para criptografar)
$ ansible-vault encrypt main.yml
# Decriptografar o arquivo
$ ansible-vault decrypt main.yml

# Criptografar para outro arquivo
$ ansible-vault encrypt main.yml --output teste.yml

# Solicita a senha de um arquivo criptografado, do contrario, nao será possivel
# executar o deploy
$ ansible-playbook -i hosts main.yml --ask-vault-pass

# Passa a senha através de um arquivo
$ ansible-playbook -i hosts main.yml --vault-password-file ./password

# Reseta a senha do arquivo criptografado 
$ ansible-vault rekey password

# Abre o arquivo para edição, sem decriptogafa-lo definitivamente
$ ansible-vault edit password

# Visualiza o arquivo, sem decriptogafa-lo definitivamente
$ ansible-vault view password
```

## Laboratório

### Laboratório 1
Montar um playbook, que instale o nginx, inici-o e habilite o boot. Copie um arquivo de index.html para um diretorio dentro da raiz do site e copie um arquivo de conf do nginx, restartando-o. O playbook deve funcioar em apenas um host.

### Laboratorio 2
Criar as instancias para o cluster k8s