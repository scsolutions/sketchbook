# Linux

## Gerenciamento de Logs

Os logs são registros de eventos no computador, ele ficam armazenados no diretório `/var/log/` e normalmente são gerenciados pelo **Rsyslog** (`/etc/ryslog.conf`). Os logs são rotacionados pelo logrotate, que faz a circulação dos arquivos em arquivos menores.

Além do Rsyslog, existem ainda o **Syslog** e o **Syslog-ng**. O Rsyslog, além de ser mais novo, se difere do syslog por permitir recursos como suporte a envio de logs remotos com TCP e UDP, logs com criptografia e armazenamento de log em banco de dados. O syslog-ng, possui os mesmos recursos do Rsyslog, no entanto, ele existe em versão enterprise.   

Os logs são configurados como *Facilidade.Prioridade*: A **facilidade** diz respeito ao tipo de serviço que será "logado", como autenticação, kernel, user, mail, etc. A **prioridade**, é o nível de criticidade (grau de detalhamento) da informação do log, como erro, debug, emerge, warn, etc. Os **arquivos** são onde os logs serão armazenados e podem ser definidos no arquivo de configuração do serviço como o `/etc/ryslog.conf`. Exs de configuração de log:

```
mail.err      /var/log/mail.err
cron.*        /var/log/cron.log
```

### Rotacionamento de arquivos
O rotacionamento de logs pode ser configurado no diretório `/etc/logrotate.d`. Exemplo de configuração:
`
/var/log/cron.log {
  daily # periodicidade
  rotate 4 # quantos arquivos ele manterá
  compress # comprime a partir da primeira rotação
  delaycompress
  size 1M # rotaciona a partir de um 1M
  missingok # cria se não houver o arquivo
  notifempty # se o arquivo estiver vazio não rotaciona
  create 644 root root # permissão dos arquivos rotacionados
}
`

### Arquivos de logs conhecidos:
- ``auth.log`` (ou security no Red Hat): arquivo de autenticação
- ``daemon.log``: serviços e daemons do sistema
- ``kern.log``: mensagens do kernel
- ``user.log``: refernte a usuários
- ``syslog e message``: todo tipo de log, vai para esses logs.
- ``local0 - local7``: reservados para uso local

### Comandos default de exibição de logs
- ``dmesg``: Exibe informações de hardware conectados ao host
- ``lastlog``: Exibe os últimos logins de cada usuário
- ``last``: Exibe os últimos logins/logouts efetuados no host
- ``lastb``: Exibe as últimas tentativas de logins mal sucedidos

### Configurar Rsyslog remoto

1. No servidor, habilitar os modulos imtcp (tcp/514) e imudp(udp/514) `/etc/rsyslog.conf`
2. Para que os logs não fiquem misturados com os da máquina local, crie o seguinte template em `/etc/rsyslog.d/template.conf`
   ```
   template (name="LogRemoto" type="string" string="/srv/log/%HOSTNAME%/%PROGRAMNAME%")
   
   # A linha seguinte, faz com que os logs locais, também caiam nesse diretório
   *.* ?LogRemoto 
   ```
3. Alterar o dono e o grupo dono do diretório de logs configurado no template.
   ```
   $ chown syslog:syslog -R /srv/log/
   ```
4. Reiniciar o serviço de **Rsyslog**
5. Configurar no gerenciador de logs da estação, o servidor remoto, por ex:
   ```
   # colocar um @ e o ip do servidor de logs, ao invés de setar o arquivo de log
   *.*                  @192.168.0.1
   # utilizando dois "@@", você força o uso do TCP para o envio de logs
   *.*                  @@192.168.0.1
   ```
6. Reiniciar o gerenciador de logs da estação

### Configurar Rsyslog remoto com criptografia

1. Instalar no servidor os pacotes `rsyslog-gnutls` e `gnutls-bin`. Nos clientes, apenas o pacote `rsyslog-gnutls`
2. Criar em todos os servidores o diretório `/etc/rsyslog-keys`
3. Gerar as chave privada e pública
   ```
   $ certtool --generate-privkey --outfile ca-ke.pem
   $ chmod 400 ca-key.pem
   $ certtool --generate-selfsigned --load-privkey ca-key.pem --outfile ca.pem
   ```
4. Nas respostas do comando anterior, configurar apenas *Common name* e *dnsName*, com o hostname do servidor
5. Ainda no comando anterior, responder sim apenas para as perguntas se o certificado é usado para assinar outros certificados e se ele é usado para assinar CRLs.
6. Gerar as chaves das estações
   ```
   $ certtool --generate-privkey --outfile estacao-key.pem --bits 2048
   $ chmod 400 estacao-key.pem
   $ certtool --generate-request --load-privkey estacao-key.pem --outfile estacao-request.pem
   ```
7. Nas respostas do comando anterior, configurar apenas *Common name* e *dnsName*, com o hostname do servidor
8. Ainda no comando anterior, responder sim apenas para as perguntas se o certificado é usado para assinar DHE.
9. Gerar os certificados das estações.
    ```
   $ certtool --generate-certificate --load-request estacao-request.pem --outfile estacao-cert.pem --load-ca-certificate ca.pem --load-ca-privkey ca-key.pem   
    ```
10. Nas respostas do comando anterior, configurar apenas *dnsName*, com o hostname do servidor 
11. Ainda no comando anterior, responder sim apenas para as perguntas se o certificado é usado para TLS web cliente e servidor.
12. Remover os certificados do tipo *request*, pois servem apenas para gerar os certificados auto assinados.
13. Todos arquivos gerados para a estação e a chave publica, devem ser enviados para a estação, no diretório `/etc/rsyslog-keys`
14. Criar o arquivo ``/etc/rsyslog.d/syslog-tls.conf``, seguindo o modelo:
    ```
    $DefaultNetstreamDriver gtls
    
    $DefaultNetstreamDriverCAFile /etc/rsyslog-keys/ca.pem
      
    $DefaultNetstreamDriverCertFile /etc/rsyslog-keys/estacao-cert.pem
    $DefaultNetstreamDriverKeyFile /etc/rsyslog-keys/estacao-key.pem

    $ModLoad imtcp
    $InputTCPServerStreamDriverMode 1
    $InputTCPServerStreamDriverAuthMode anon
    $InputTCPServerRun 6514

    :fromhost, isequal, "estacao"      /var/log/estacao/messages
    :fromhost, isequal, "estacao"      ~
    ```
15. Alterar o dono e o grupo dono do diretório de certificados
   ```
   $ chown syslog:syslog -R /etc/rsyslog-keys
   ```
16. Reiniciar o serviço do rsyslog do servidor
17. Criar o arquivo ``/etc/rsyslog.d/syslog-tls.conf``, na estação seguindo o modelo:
    ```
    $DefaultNetstreamDriverCAFile /etc/rsyslog-keys/ca.pem
    
    $DefaultNetstreamDriver gtls

    $InputTCPServerStreamDriverMode 1
    $InputTCPServerStreamDriverAuthMode anon
    
    *.* @@(o)graylog:6514
    ```
18. Reiniciar o serviço do rsyslog da estação

### Configurar Rsyslog com Mysql

1. Instalar o MYSQL
2. Criar o banco de dados com o nome *Syslog*
3. O Mysql possui um template para a criação das tabelas do Syslog. Além disso é necessário criar o usuário de acesso ao banco e dar privilégios.
   ```
   $ mysql -u root -D Syslog < /usr/share/dbconfig-common/data/rsyslog-mysql/install/mysql
   $ mysql -u root -D Syslog -e "CREATE USER rsysloguser@localhost IDENFIFIED y 'PASS';"
   $ mysql -u root -D Syslog -e "GRANT ALL ON Syslog.* to rsysloguser@localhost IDENTIFIED by 'PASS';"
   ```
4. Configurar o Rsyslog, editando o arquivo `/etc/rsyslog.d/mysql.conf`, alterando o usuário e senha.
   

### Auditd
Faz a auditoria através de logs e regras, para acesso e alteração de arquivos. O comando pode ser encontrado no pacote ``auditd``.

**Comandos do auditd**     
```
# Lista regras de auditoria
$ auditctl -l

# Exibe o status do serviço
$ auditctl -s

# Configura uma auditoria. No ex abaixo, o filtro está auditando as mudanças 
# de [r]ead, [w]rite, e[x]ecute e [a]tributo. O resultado pode ser conferido
# no log do auditctl.
$ auditctl -w /etc/passwd -p rwxa

# Remove a regra criada
$ auditctl -W /etc/passwd  

# É possível auditar system calls utilizando o comando abaixo
$ auditctl -a always,exit -F arch=b64 -S clock_settime -F key=mudarhora
```

**Gravar as regras**      
```
# 1- Incluir uma chave com o parâmetro ``-k``
$ auditctl -w /etc/passwd -p rwxa -k listausuarios

# 2- Listar as regras e redireciona-las para o arquivo de configuração.
$ audtictl -l >> /etc/audit/rules.d/audit.rules
```
> É possível exibir as system call através do comando ``ausyscall --dump``

**Busca e relatório**     
```
# Busca as regras que deram match, pelas palavras chaves
$ ausearch -k listausuario

# Busca pelo comando executado
$ ausearch -x /usr/bin/crontab

# Relatorio resumido de auditoria
$ aureport --sumary
```

### Graylog
Software de gerenciamento de logs, o Graylog é utilizado juntamente com o mongoDB e Elasticsearch. É possível montar clusters de logs, enviar alertas, dashboards, etc.

**Instalação**
1. Instalar os pré-requisitos
   ``$ apt-get install apt-transport-https openjdk-8-jre-headless uuid-ruintime pwgen``
2. Configurar o Java Home, incluindo a linha abaixo no arquivo ``/etc/environment``. Atualizar em seguinda com o comando ``source``.
   ```
   JAVA_home="/usr/lib/jvm/java-1.8.0-openjdk-adm64/bin/"

   $ source /etc/environment
   ```
3. Instalar o MongoDb (testado na versão 4.0, mais informações [aqui](https://docs.mongodb.com/v4.0/tutorial/install-mongodb-on-ubuntu/))
4. Instalar o Elasticsearch (testado na versão 6.8, mais informações [aqui](https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html))
5. Editar o arquivo ``/etc/elasticsearch/elasticsearch.yml``, incluindo o nome do host na linha ``cluster.name``
6. Instalar o Graylog, mais informações (testado na versão 3.3)[aqui](https://docs.graylog.org/en/4.0/pages/installation/operating_system_packages.html)
7. Gerar um hash para a senha, ela será configurada no arquivo de configuração do Graylog.
   ```
   $ echo -n <senha> | sha256sum
   ```
8. Gerar a senha para os usuários. Utilize o ``pwgen``
   ````
    $ pwgen -N 1 -s 96
    ```
9.  Editar as linhas ``password_secret`` (passo 8), ``root_password_sha2`` (passo 7), ``http_bind_address``, ``http_publish_uri`` e ``http_external_uri``.
10. Acesse o Graylog pelo endereço configurado em ``http_bind_address``. O usuário default é *admin*. A senha utilizada é a mesma do comando do passo número 7.
11. Adicione no servidor um Input: **Graylog> System/Inputs> Inputs** do tipo Syslog TCP ou UDP conforme preferência.
12. Nas estações, adicione a seguinte linha em ``/etc/rsyslog.conf``. Substituindo *example* pelo servidor Graylog.
    ```
    *.* @graylog.example.org:514;RSYSLOG_SyslogProtocol23Format
    ```
> É possível ler logs de containers adicionando o input do tipo *GELF*. O docker possui o [driver GELF](https://docs.docker.com/config/containers/logging/gelf/), para permitir as conexões.

### Journalctl
Ferramenta de gerenciamento de logs do journald, que é o serviço de monitoramento de logs do systemd.

**Filtro por tempo**   
```
# Faz a busca de dez dias atrás, até uma hora atrás
$ journalctl --since "10 days ago" --until "1 hours ago"

# Faz a busca pela data exata, de 01 de Janeiro, até 16 de Janeiro.
$ journalctl --since "2021-01-01 01:00:00" --until "2021-01-16 01:00:00"

# -r faz o filtro reverso, logs mais novos primeiro
$ journalctl -r --since "2021-01-01 01:00:00" --until "2021-01-16 01:00:00"
```

**Filtro de logs**
```
# kernel
$ journalctl -k

# por unidade
$ journalctl -u ssh
$ journalctl -u docker

# exibe as três ultimas mensagens
$ journalctl -u nginx -n 3
```

**Busca por level**
```
$ journalctl -p err
$ journalctl -p debug
```

**Por usuário**
```
journalctl _UID=1000
```

**Log em tempo real**
```
$ journalctl -f
```

**Exibe em modo JSON**
```
$ journalctl -o json-pretty
```

## Gerenciamento de permissões (ACL)

primeiro caracter, tipo do arquivo

- arquivo comum
d diretorio
l link simbolico
b dispositivo de bloco
c dispositivo de caracter
s socket

dono

grupo

outros

rwx permissões, write, read, execute

sinal de + no fim do arquivo, tem acl

### Permissões
chmod g+r arquivo
chmod g=r arquivo
chdmod o+wx arquivo

chmod +x arquivo
chmod -x arquivo

4 read
2 write
1 exeute

chmod -R (recursivo)

### Muda o dono e grupo
Modifica dono e grupo dono do arquivo 

chown user arquivo
chown :group arquivo
chown user:group arquivo

chgrp modifica grupo dono do arquivo
chgrp group file

### Permissões especiais
suid = 4
Todos usuários conseguem executar o arquivo

sgid = 2
Qualquer grupo, com as mesmas permissões do grupo dono 

stick bit = t
todo os arquivos e abaixo dele, só o dono consegue apagar ou renomear

### Setfacl e grpfacl
setfacl -m user:usuario:rwx arquivo
setfacl -m group:grupo:rwx arquivo
getfacl arquivo


## Gerenciamento de pacotes

### DPKG vs APT

 **Características**
DPKG|APT
----|----
-Utilizado em distribuições Debian e derivados como Ubuntu<br/>- Faz a instalação de arquivos `.deb`<br/>- É necessário o download "manual" do pacote.<br/>- Não faz a instalação de dependências<br/>|<br/>- Utilizado em distribuições Debian e derivados como Ubuntu<br/>- Faz a instalação de arquivos `.deb`<br/>- Instala dependencias dos pacotes.<br/>- Precisa de um repositorio para instalação dos pacotes configurado em `/etc/apt/sources.list`<br/>- Armazena os pacotes em `/var/cache/apt/archive/`

```
# Instala um pacote
$ dpkg -i pacote

# Verifica se um pacote está instalado
$ dpkg -l pacote

# Remove o pacote, porém deixando resquício
$ dpkg -r pacote

Purge, remove completamente o pacote
$ dpkg -P pacote

# Visualiza conteúdo do pacote
$ dpkg -c pacote

# Exibe informações do que foi instalado
$ dpkg -L pacote

# Exibe informações detalhadas do pacote
$ dpkg -I pacote
```

### APT
- Utilizado em distribuições Debian e derivados como Ubuntu
- Faz a instalação de arquivos `.deb`
- Instala dependencias dos pacotes.
- Precisa de um repositorio para instalação dos pacotes configurado em `/etc/apt/sources.list`
- Armazena os pacotes em `/var/cache/apt/archive/`

**Comandos**
```
# Atualiza a lista de pacotes do repositorio 
$ apt-get update

# Atualiza os pacotes instalados (não recomendado em ambiente de produção)
$ apt-get upgrade

# Atualiza a distribuição
$ apt-get dist upgrade

# Instala um pacote
$ apt-get install pacote

# Remove um pacote
$ apt-get remove pacote

# Purge (remove e limpa) um pacote
$ apt-get remove --purge pacote

# Procura um pacote
$ apt-cache search

# Exibe informações de um pacote
$ apt-cache show

# Lista dependencias de um pacote
$ apt-cache depends pacote

# Estatisticas do repositorio
$ apt-cache stats

# Limpa os pacotes .deb instalados baixados do archive
apt-get clean
```

### RPM
Não resolve dependências

rpm ivh pacote.rpm
-instalar, -verbose, -humanable (barrinha)
yum install pacote

Verifica todos os arquivos que estão instalados ou por um especifico
rpm -qa
rpm -qa pacote

Exibe os ultimos pacotes instalados
rpm -qa --last 

Arquivos que o pacote tem
rpm -ql pacote

Busca o reponsavel por instalar determinado programa
rpm -qf pacote

Mais informações sobre o pacote
rpm -qi pacote
yum info htop

Remove pacote
rpm -e pacote
yum remove pacote

Update do pacote
rpm -Uvh pacote
yum update pacote

Verifica se existe dependências para o pacote
rpm -qpR pacote

Faz auditoria, verifica a integridade do pacote (Timestamps, MD5 checksum e File sizes)
rpm -Va 
```

### YUM
Repositorio do yum
/etc/yum.repos.d/

Valida os repos do Yum
yum repolist

Limpa o dir de pacotes que sao baixados
yum clean all

# Atualiza a distribuição o CentOS
yum upgrade

Usa repositorio
Utiliza o rpm em sua base para instalar pacotes
Resolve dependencias
Instala grupo de pactotes, suites de pacotes

suites de pacotes
yum groupinstall webserver
yum groupinstall dns "name server"

Busca por um pacote (procura por parte do nome)
yum search mysql

Plugin para realizar o download sem fazer a instlaação do mesmo
yum install package


Instala um pacote local
yum localinstall pacote.rpm 

Downgrade do pacote
yum downgrade pacote

### Compilação de pacotes

Para compilar pacotes, é necessário ter os seguintes pacotes instalados:
```
gcc gcc-c++ make
```

1. Download do fonte
2. README/ INSTALL
#Verifica os requisitos
3. ./configure
# Automatiza o processo de instalaçãõ
4. make
5. make install


Correção das dependencias
`make clean`

## Gerenciamento de procesos
Qualquer programa em execução, recebe um PID, um identificador de um processo
Todos possuem um lifetime, isto é um tempo de execução

PPID (init) é processo pai, que deu origem ao filho, seu PID é 1

UID Usuário responsável pelo processo

### PS 
Visualiza processos no Linux, o PS não é dinamico

ps aux
a todos os processos
x todos os processos que não estão atrelados aquele terminal
u hrario e usuarios
> Acrescentando o www apos o aux, ele traz o comando inteiro da coluna COMMAND

Coluna RSS quantidade de memoria fisica do processo

Coluna Time, tempo total de CPU

Coluna TTY, se está atrelado a um terminal

Coluna Stat status do processo
R Running processo que esta execução, está rodando
S Sleep Processo ativo mas que não está sendo executado
Z Processo que fica consumindo recurso, porém não tem mais utilidade, muitoas vezes ocorre com processos que perdem o processo pai
T Stopped, processo que foi interrompido

ps -ef

> O comando `pstree`, exibe a árvore de processos

Busca o id do processos
$ pidof crond
$ pgrep crond

### TOP
Informações dos processos em tempo real. O < e > pagina os processos.

K para finalizar o processo, colocando o numero do PID

### HTOP
Top mais interativo

### Kill
Comando para finalizar e enviar sinais para outros processos

`pgrep processo` retorna o pid do processo
kill pid (signal 15) encerra corretamente, é o processo padrão
kill -9 pid encerra forçadamente
kill -1

SIGHUP (1): Reinicia o processo 
SIGHUP (2): Interrompe o processo
SIGHUP (3): Fecha o processo
SIGHUP (9): Finaliza o processo imediatamente
SIGHUP (15): Solicita o término do processo

> O comando `killall` mata todos os processos, enquanto o `pkill` faz o mesmo processo, mas sem a necessidade de colocar o nome exato do processo

### Gerenciando processos
A combinação `Ctrl + Z` coloca o processo em segundo plano.

O comando `jobs`, lista processos que estão em segundo plano. 
`[1] vim arquivo.txt`

O comando `fg` combinado ao número do job, "traz" o processo novamente para o primeiro plano.
`fg 1`

> Incluindo o sinal `&` ao final do comando, o processo iniciará em segundo plano.

O comando `nohup` executa comandos e ignora os sinais de conexão, redirecionando a saida para um arquivo. Combinado com o parâmetro `&`, o processo fica continuamente rodando em background, mesmo se o usuário sofrer logout.

### Alterando as prioridades dos 
As prioridades podem variar de -20 (maior prioridade) até 19 (menor prioridade). A prioridade padrão de processos executados pelo root são 0, enquanto por usuários comuns é 10.

Para rodar um processo com prioridade alterada, utilizar o comando `nice`, processos já em execução, precisam ser alterados com o comando `renice`.


### Find e Locate
Locate não busca diretamte no sistema operacional, ele utiliza o updatedb. O locate por default não procura no /tmp

find path -name arquivo
find / -name bola.txt
find / -iname Bola.txt # Não é case sensitive
find / -iname Bol* # usa meta caracteres
find / -iname Bola.txt -user root -group root -perm 640 # Filtra as opções por usuário e grupo
find / -iname Bola.txt -type f # procura por tipo de arquivo    
find / -iname Bola.txt -atime -30 # procura por arquivos acessados a 30 dias
find / -iname Bola.txt -mtime -10 # procura por arquivos modificados a 10 dias
find / -iname Bola.txt -ctime +15 # ??

find / -iname Bola.txt -exec echo "Ola" # executa um comando ao achar o arquivo

VER FD-find

### DF
Visualiza as informações de disco
df -h
df -i verifica os inodes

### nestat
Exibe as portas em listen e estabilished
netstat -atunp 

Exibe as portas em listen
netstat -nltp

### dd
dd if=/dev/zero of=/home/root/arquivo bs=1M count=1024

## Volumes

### LVM
Os volumes LVM podem ser expandidos ou reduzidos, através de várias partições e discos diferentes, para que sejam interpretados como apenas um único volume. Os volumes podem ser aumentados ou diminuídos sem haver perda de dados.   

- **PV**: Physical volume, indica o disco, hd ou partição.   
- **VG**: Volume group, a reunião dos discos ou partições em um unico volume
- **LV**: Logical group, partições de um volume group

**Comandos**
```
# Exibe os physical volume
$ pvs
$ pvdisplay

# Exibe os volume group
$ vgs 
$ vgdisplay

# Exibe os logical volume
$ lvs
$ lvdisplay
```

**Criando um LV**
```
# Inicializa o disco no LVM
$ pvcreate /dev/sdb

# Cria o volume group com os pvs adicionados
$ vgcreate <vgname> <pv1> <pv2> <pvN...>
$ vgcreate vgdados /dev/sdb /dev/sdc

# Cria o lv no vg
$ lvcreate -L <tam> -n <lvname> <vgname>
$ lvcreate -L 5G -n lv_usr vgdados
```
> Após a criação, os logical volume devem ser formatados como um disco normal para que depois ele seja montado.

**Aumentando e diminuindo VGs**
```
$ vgextend <vgname> <pv1>
$ vgreduce <vgname> <pv1>
```
> Não esquecer de adicionar antes o pv, com o comando ``pvcreate``.

**Aumentando e diminuindo LVs**
```
$ lvextend -L <novotam> <pathdolv>

# Aumentando o LV
$ lvextend -L +5G /dev/vgdados/lv_usr

# Diminuindo o LV
$ lvreduce -L -2G /dev/vgdados/lv_usr
```
> Quando o volume é **extendido**, é necessário rodar o comando `resize2fs <pathdolv>` para redimensionar a partição. Quando o volume é **reduzido** o comando `resize2fs <pathdolv> <novotam>` deve ser rodado antes do ``lvextend``.

**Remover o LV**
```
$ lvremove /dev/vgdados/lv_usr
```

### du
du -hs * # tamanho dos diretorios

## TRICK TRIck

Lista os arquivos abertos no sistema
$ lsof

grep -v "^$" inicio e fim de linha

### Rsync
# Atualiza a arvore e copia apenas o que mudou
rsync -u

# Preserva as permissões originais
rsync -p

### ssh
Gerar o par de chaves
$ ssh-keygen

Copia para o servidor remoto 
$ ssh-copy-id root@ 192.68.0.1

### history
history # historico dos ultimos 1000 comandos
history | tail # ultimos 10 comandos

> !* todos os argumentos do comando anterior
> ALT+T alterna os ultimos argumentos
> Ctrl + R search nos comandos

## Kernel

strace
strace echo "oi" > oi
strace -e openat -t find / -iname *.ko

## Sysdig

## For Red Hat or CentOS:

### Check the time:

> timedatectl status

> List all available timezones:
timedatectl list-timezones

> Set up your timezone:
sudo timedatectl set-timezone Europe/Paris