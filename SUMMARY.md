## Sumário
- [Introdução](README.md)

<!-- TOC -->
- [Docker](docker/Docker.md#docker) 
  - [Instalando o Docker](docker/Docker.md#instalando-o-docker)
  - [Comandos do Docker](docker/Docker.md#comandos-do-docker)
  - [Docker File e Imagens](docker/Docker.md#docker-file-e-imagens)
  - [Volumes](docker/Docker.md#volumes)
  - [Docker Hub](docker/Docker.md#docker-hub)
- [Docker Machine](docker/Docker.md#docker-machine)
- [Docker Swarm e Services](docker/Docker.md#docker-swarm-e-services)
  - [Swarm](docker/Docker.md#swarm)
  - [Services](docker/Docker.md#services)
  - [Network](docker/Docker.md#network)
  - [Secrets](docker/Docker.md#secrets)
- [Docker Compose e Stack](docker/Docker.md#docker-compose-e-stack)
  - [Comopose](docker/Docker.md#compose)
  - [Stack](docker/Docker.md#stack)
- [Laboratórios](docker/Docker.md#laboratorios-under-construction)
<!-- TOC -->