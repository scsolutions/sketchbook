ElasticSearch /  OpenSearch


Data NODE = armazena e processa os dados, responde as consultas e fazem a indexação dos dados
Master NODE = garante a replicação e orquestra o nó
shard = grupo de informações

Design do Elastic Search
- Search Workloads: Basicamente carrega os dados para serem pesquisados.

- Analytics Workloads: Disponibilidade quase em tempo real de logs e dashboards e tecnologias de monitoramento, para saber como está a saúde da infraestrutura.

Não é recomendado como armazenamento durável, como banco de chave valor e não é recomendável para consultas que tragam um número grande de resultados.

É recomendado para consultas de baixa latência e de alta taxa de transferência dos dados.

curl -XGET https://gco-es-pesquisa-prd.infra.grancursosonline.com.br/_cat/shards?v

As instancias não conseguem fazer duas coisas ao mesmo tempo

indicios q o cluster jvm ruim


LINKS
======

Msg de erro
https://sa-east-1.console.aws.amazon.com/esv3/home?region=sa-east-1#opensearch/domains/gco-pesquisa-prod/DomainNotifications/1eadd529-49ff-4169-af69-9b073bff756d
https://docs.amazonaws.cn/en_us/opensearch-service/latest/developerguide/monitoring-events.html

Recomenda
* https://aws.amazon.com/pt/blogs/big-data/increase-availability-for-amazon-opensearch-service-by-deploying-in-three-availability-zones/
* https://aws.amazon.com/pt/blogs/big-data/best-practices-for-configuring-your-amazon-opensearch-service-domain/
https://aws.amazon.com/pt/blogs/database/get-started-with-amazon-elasticsearch-service-how-many-data-instances-do-i-need/
https://aws.amazon.com/pt/blogs/database/get-started-with-amazon-elasticsearch-service-use-dedicated-master-instances-to-improve-cluster-stability/


Shards
https://aws.amazon.com/pt/blogs/database/get-started-with-amazon-elasticsearch-service-how-many-shards-do-i-need/
https://aws.amazon.com/premiumsupport/knowledge-center/opensearch-rebalance-uneven-shards/?nc1=h_ls
https://docs.aws.amazon.com/opensearch-service/latest/developerguide/sizing-domains.html


Apresentação
https://pages.awscloud.com/rs/112-TZM-766/images/2020_0720-ABD_Slide-Deck.pdf


Métricas
https://sa-east-1.console.aws.amazon.com/cloudwatch/home?region=sa-east-1#metricsV2:graph=~(view~'timeSeries~stacked~false~region~'sa-east-1~start~'2022-07-01T03*3a00*3a00.000Z~end~'2022-07-08T02*3a59*3a59.000Z~metrics~(~(~'AWS*2fES~'ShardCount~'ShardRole~'Replica~'DomainName~'gco-pesquisa-prod~'NodeId~'-yBJP_xcSaO21MlX4b4PUw~'ClientId~'130411430258)~(~'...~'Primary~'.~'.~'.~'.~'.~'.)~(~'...~'2D1liXLnSLCgWdCUlDRkUg~'.~'.)~(~'...~'Replica~'.~'.~'.~'.~'.~'.)));query=~'*7bAWS*2fES*2cClientId*2cDomainName*2cNodeId*2cShardRole*7d

https://sa-east-1.console.aws.amazon.com/cloudwatch/home?region=sa-east-1#metricsV2:graph=~(view~'timeSeries~stacked~false~region~'sa-east-1~start~'2022-07-01T03*3a00*3a00.000Z~end~'2022-07-08T02*3a59*3a59.000Z~metrics~(~(~'AWS*2fES~'ShardCount~'ShardRole~'Primary~'DomainName~'grancursos-ecs-logs~'NodeId~'BUiYLqwSTpu5YtjR7n-1Vg~'ClientId~'130411430258)~(~'...~'FtP3qlI7QGuK_0B1NlTchw~'.~'.)~(~'...~'ubfKcWS1RS-gxC1mXRMR6g~'.~'.)~(~'...~'ZdyDQ-8_TNOvebAFc04pBA~'.~'.)));query=~'*7bAWS*2fES*2cClientId*2cDomainName*2cNodeId*2cShardRole*7d
---------------------------------------

curl -XPUT https://vpc-gco-pesquisa-prod-dr-s3erh7z67jqtoblui53b7xgypq.sa-east-1.es.amazonaws.com/


curl -XPUT 'https://vpc-gco-pesquisa-prod-dr-s3erh7z67jqtoblui53b7xgypq.sa-east-1.es.amazonaws.com/_snapshot/repo-dr' 
{
  "type": "s3",
  "settings": {
    "bucket": "grancursos-elasticsearch-gco-pesquisa-prod-bkp-daily-dr",
    "region": "us-east-1",
    "role_arn": "arn:aws:iam::557595946399:role/TheSnapshotRole"
  }
}


cat <<EOF | curl -XPUT 'https://vpc-gco-pesquisa-prod-dr-s3erh7z67jqtoblui53b7xgypq.sa-east-1.es.amazonaws.com/_snapshot/repo-dr' 
{
  "type": "s3",
  "settings": {
    "bucket": "grancursos-elasticsearch-gco-pesquisa-prod-bkp-daily-dr",
    "region": "us-east-1",
    "role_arn": "arn:aws:iam::557595946399:role/TheSnapshotRole"
  }
}
EOF

opensearch-cli curl put -P _snapshot/repo-dr -d @teste.json --profile aws


curl -H 'Content-Type: application/x-ndjson' -XPOST 'https://vpc-gco-pesquisa-prod-dr-s3erh7z67jqtoblui53b7xgypq.sa-east-1.es.amazonaws.com/_snapshot/repo-dr' --data-binary '@teste.json'

COMANDOS
=========
NOVO ENDPOINT
https://vpc-elasticsearch-gcq-prd-dr-izn6wl2rwagnuge3j5t7ds4mwq.us-east-1.es.amazonaws.com

NUMERO DE SHARDS
curl -XGET https://vpc-elasticsearch-gcq-prd-dr-izn6wl2rwagnuge3j5t7ds4mwq.us-east-1.es.amazonaws.com/_cat/shards?v

IDENTIFICAR INDICES
curl -XGET https://vpc-elasticsearch-gcq-prd-dr-izn6wl2rwagnuge3j5t7ds4mwq.us-east-1.es.amazonaws.com/_cat/indices?v

CONSULTAR INDICES (alterar para o indice a ser consultado)
curl -XGET https://vpc-elasticsearch-gcq-prd-dr-izn6wl2rwagnuge3j5t7ds4mwq.us-east-1.es.amazonaws.com/ranking-log-2022.08.16

VER OS Snapshot
curl -XGET https://vpc-gco-pesquisa-prod-dr-on3vtbcsvg452jqb62ypws2ori.us-east-1.es.amazonaws.com/_snapshot/_status

IDENTIFICAR OS REPOS DE SNAP
curl -XGET https://vpc-elasticsearch-gcq-prd-dr-izn6wl2rwagnuge3j5t7ds4mwq.us-east-1.es.amazonaws.com/_snapshot?pretty

VER OS SNAPSHOT DO REPO (alterar para o nome do repo)
curl -XGET https://vpc-gco-pesquisa-prod-dr-on3vtbcsvg452jqb62ypws2ori.us-east-1.es.amazonaws.com/_snapshot/repo-dr/_all?pretty

RESTARURAR SNAPSHOT (alterar o nome do repositorio e do backup que deseja restaurar )
curl -XPOST https://vpc-gco-pesquisa-prod-dr-on3vtbcsvg452jqb62ypws2ori.us-east-1.es.amazonaws.com/_snapshot/repo-dr/2022-08-25/_restore

REMOVER OS INDICES ANTERIORES
curl -XDELETE https://vpc-gco-pesquisa-prod-dr-on3vtbcsvg452jqb62ypws2ori.us-east-1.es.amazonaws.com/_all


LINKS
=====
https://stackoverflow.com/questions/48124737/parse-exception-request-body-is-required
https://aws.amazon.com/pt/premiumsupport/knowledge-center/opensearch-restore-data/
https://aws.amazon.com/pt/premiumsupport/knowledge-center/opensearch-migrate-domain/
https://docs.aws.amazon.com/opensearch-service/latest/developerguide/managedomains-snapshots.html#managedomains-snapshot-registerdirectory
https://opensearch.org/docs/latest/opensearch/rest-api/cat/cat-indices/
https://stackoverflow.com/questions/69302123/elasticsearch-restore-snapshot-in-a-new-single-node-cluster