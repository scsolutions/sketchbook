<img src="../images/docker-logo.png" alt="Docker" width=128>   

# Docker

## Sumário

<!-- TOC -->
- [Docker](#docker) 
  - [Instalando o Docker](#instalando-o-docker)
  - [Comandos do Docker](#comandos-do-docker)
  - [Docker File e Imagens](#docker-file-e-imagens)
  - [Volumes](#volumes)
  - [Docker Hub](#docker-hub)
- [Docker Machine](#docker-machine)
- [Docker Swarm e Services](#docker-swarm-e-services)
  - [Swarm](#swarm)
  - [Services](#services)
  - [Network](#network)
  - [Secrets](#secrets)
- [Docker Compose e Stack](#docker-compose-e-stack)
  - [Comopose](#compose)
  - [Stack](#stack)
- [Laboratórios](#laboratorios-under-construction)
<!-- TOC -->

## Docker
O Docker é um gerenciador de containers, podem ser considerados uma abstração de uma camada de aplicação, em contrapartida, uma VM seria a abstração de uma máquina física. 

Os **containers** são imagens, aplicações e microssistemas que compartilham o kernel do sistema operacional base (host), no entanto eles possuem a característica de possuírem suas bibliotecas e binários isoladamente. Diferentemente de uma VM, os serviços são mais enxutos e não dependem de hypervisor ou de um sistema para cada máquina, economizando assim recuros do host.

<img src="../images/Container-vs-VMs.jpg" alt="VM vs Docker" width="431" height="244">

As imagens do Docker são feitas de camadas e utilizam basicamente apenas o core do sistem, elas são armazenadas em Registers ou Registradores, como o DockerHub, também é possível criar **registers** locais, para a distribuição de imagens localmente. Um container deve ser efêmero e imutável, isto é, descartável, ele nasceu para morrer e não deve ficar passando por mudanças de configuração ou armazenando informações.

### Instalando o Docker

A versão Docker CE (Community Edition) é gratuíta e pode ser instalado no Windows, Linux ou macOS, [consulte a plataforma de instalação](https://docs.docker.com/engine/install/) que você deseja. Existe ainda a versão EE (Enterprise Edition) que é paga.

A instalação no Linux (testado no Ubuntu 18.04 e 20.04) pode ser executada rodando apenas os seguintes comandos:
```
## Atualiza a lista de pacotes
$ apt-get update
## A URL a seguir é um script em python, ele identifica qual é a distribuição 
## do host, adiciona o endereço no repositório e instala via apt-get.
$ curl -fsSL https://get.docker.com | bash
```

> Por padrão, o usuário *root* é quem detém as permissões para executar os comandos do Docker. Para que um usuário do sistema execute também em modo privilegiado, utilize o comando `sudo usermod -aG docker <user> ; newgrp docker`

Para testar se a instalação foi bem sucedida, execute os seguintes comandos:
```
$ docker version
## Para subir um container de teste
$ docker run container hello-world

```

> O [Play with Docker](https://labs.play-with-docker.com/) é uma inciativa que "emula" o Docker e pode ser acessado online. É possível rodar comandos, containers e até subir clusters, sem a necessidade de instalar a engine. Ótima solução para testes, estudos e laboratórios.

### Comandos do Docker

A partir da versão 1.13, os comandos do Docker passaram a adotar a sintaxe `docker COMMAND SUBCOMMAND`, a sintaxe antiga, será descontinuada, mas também haverá exemplos.

Mais informações sobre os comandos, no [guia oficial de referência de comandos](https://docs.docker.com/engine/reference/commandline/docker/).

**Iniciando um novo container**.
O parâmetro `-t` é utilizado para alocar um pseudo terminal tty e o `-i` aguarda a interação do usuário, isto é, para ter interatividade com o shell. O último argumento faz referência a imagem que o container irá utilizar. O parâmetro `-d` deixa o container rodando em modo daemon, isto é em background. A lista completa pode ser consultada no [site do Docker Hub](https://hub.docker.com/). Uma imagem pode ser utilizada de outros locais, ver [imagens](#imagens).

``` 
$ docker container run -ti ubuntu
# Executa o container e o deixa rodando em background
$ docker container run -d alpine    
## Versão antiga
$ docker run -ti centos
```

> A sequência **Ctrl + P + Q**, sai de um container sem pará-lo.   
> A sequência **Ctrl + D**, sai do container e o interrompe.

**Listando os containers**   
 O paramentro `-a` ´utilizado para exibir todos os containers, mesmo os parados.

```
$ docker container ls
# Versão antiga
$ docker container ps -a
```

**Atachando ao container**   
Utilize o subcomando `attach` para atachar a um container que estiver rodando.
```
$ docker container attach <iddocontainer> 
```

**Rodando comandos no container**   
É possível rodar comandos no container, sem estar dentro dele.
```
## Rodando um comando
$ docker container exec <id ou nome> <comando>
## É possível rodar um bash em um container que não possua terminal interativo.
$ docker continaer exec -it nginx-server bash
## É possível também listar um arquivo.
$ docker container exec -it nginx-server ls /etc/issue
```

**Iniciar, parar e pausar containers**   
```
## Cria um container mas não o executa  
$ docker container create <imagem>

## Rodar um container parado
$ docker container start <iddocontainer> 
$ docker start <iddocontainer>

## Parar um container
$ docer container stop <iddocontainer>
$ docker stop <iddocontainer>

## Reiniciar um container
$ docker container restart <iddocontainer>
$ docker restart <iddocontainer>

## Pausar um container
$ docker container pause <iddocontainer>
$ docker pause <iddocontainer>

## Tirar a pausa de um container
$ docker container unpause <iddocontainer>
$ docker unpause <iddocontainer>
```

**Remove conatainers**   
O parâmetro `-f` pode ser utilizado para forçar a remoção.
```
$ docker container rm <iddocontainer>

## O subcomando `prune`, remove parados, isto é nativo. Tome cuidado!
$ docker container prune
```

**Informações dos containers**   
O subcomando `inspect` é utilizado para exibir informações de um container em formato XML. É possível obter informações detalhadas como ip, volume, etc.

O subcomando `stats`, exibe informações de consumo do container, como processamento, memória, I/O, etc.

O subcomando `top`, exibe os processos que o container está rodando.

O subcomando `update`, atualiza configuações do container, como limite de cpus, memória, etc

```
$ docker container inspect <iddocontainer>

$ docker container stats <iddocontainer>

$ docker container top <iddocontainer>

$ docker container update <opções> <iddocontainer>
## Exemplo de utilização, alterando para utilizar 20% da CPU e 256 de memória
$ docker container update --cpus 0.2 -m 256m
```

> Uma forma de no Linux simular a carga nos containers, é através do comando `stress`.
> ```
> $ apt-get update ; apt-get install install stress
>
> ## No comando abaixo, o `stress` coloca carga simulando 1 cpu, utilizando 
> ## 128M de memória em apenas uma máquina
> $ stress --cpu 1 --vm-bytes 128 --vm1
> ```

**Logs**   
O subcomando ´logs´ exibe os logs do container, utilizando o parâmetro `-f`, faz seguir o log em tempo real.

```
$ docker container logs -f <iddocontainer>
```

### Docker File e Imagens

O Docker file é um arquivo texto com informações, instruções e comandos utilizados, sobre uma imagem que será buildada (construída). A imagem nada mais é que um container parado.

O Docker file é utilizado para construir imagens customizadas do Docker e sua montagem segue basicamente o critério de instrução-argumento.

**Arquivo de exemplo do dockerfile**
```
# # O primeiro argumento do DockerFile é o FROM, onde você informa a imagem 
## base que será utilizada na criação da imagem
FROM Ubuntu

## Insere metadatas e comentarios ao dockerfile como versão, descrição e 
## fabricante. Usa o criterio chave = valor"
LABEL APP_VERSION="1.1.0"
#
# Insere variáveis de ambiente
ENV NPM_VERSION=8

## ENTRYPOINT: É o principal processo da execução do container, se ele parar o 
## container para, é o init do  container. Permite configurar um executável 
## para o container. Existem duas formas de passa-los:
## ex1 (Modo exec)
ENTRYPOINT ["/usr/sbin/apachectl"]
## ex2 (Modo Shell)
ENTRYPOINT /usr/sbin/apachectl

## CMD: Roda comandos quando o container for inciado, só pode existir um CMD  
## por dockerfile.  
## Quando o ENTRYPOINT não existe, ele o substitui. Quando o ENTRYPOINT existe, o 
# CMD se torna um parametro para o processo principal e é utilizado para a passagem de argumentos, configurados
# no ENTRYPOINT.
# ex1 (Modo exec)
CMD ["-D", "FOREGROUND"]
# ex2 (Modo shell)
CMD stress --cpu 1 --vm-bytes 128M --vm 1

## RUN: Serve para rodar comandos e instruções durante o BUILD (construção) da 
## imagem do container.


RUN apt-get install update
RUN apt-get install -y vim

## Ainda sobre RUN
## Boas práticas dizem para concatenar os comandos por causa das camadas de RUN,
## o comando anterior não apaga o comando seguinte. O espaço continua 
## alocado, mesmo que você tente limpar na camada seguinte.
RUN apt-get install update && apt-get install -y vim && apt-clean cache

## WORKDIR: Define um diretório de trabalho aonde as instruções e os comandos 
## RUN, CMD, ENTRYPOINT e AND, serão executados.
WORKDIR /usr/share/myapp
RUN npm build   

## COPY: copia arquivos locais para dentro das imagens, do servidor ou estação 
## local para o destino (workdir)
COPY .files/requiriments.txt requiriments.txt

## ADD: Tem a mesma utilização do COPY mas tem capacidade de descompactar 
## arquivos diretamente no destino e baixar URLs 
ADD files.tar.gz ./

## Porta que deve ser publicada (mapeada), para que o serviço funcione
EXPOSE 80

## Mapeamento de volumes para o container.
VOLUME [ "/usr/share/nginx/html" ]

## Usuario padrão do container. Default é o root, dependendo da imagem utilizada.
USER www-data

## Diretorio default (iniciado) quando o container subir
WORKDIR /var/www/html

## Ponto de montagem para o container
VOLUME /var/www/html
```

**Buildar (construir) uma imagem**
```
$ docker image build -t <nome:versao> .
```
> O parâmetro --no-cache faz com que a imagem de origem seja baixada novamente na hora do build, mesmo que ela já exista no host. Pode ser útil em casos onde houve atualização de algum pacote.

**Listar imagens**
```
$ docker image ls

# Forma antiga
$ docker images
```

**Remover image**
```
$ docker image rm <imagem>
$ docker rmi <imagem>
```
> A imagem só pode ser removida se não estiver sendo utilizada, ainda assim é possivel utilizar o argumento `-f` para forçar a remoção.

**Informações da imagem**
```
$ docker image inspect <imagem>

## Exibe o historico da imagem
$ docker image history <imagem>
```

**Download da imagem**
```
$ docker pull <imagem>
```

**Multi Stage**
Cria um pipeline no build da imagem, útil para deixa-la mais enxuta e otimizada, facilitando a sua manutenção e gerenciamento. O multi stage permite o uso de mais de um from no docker file, criando uma interação entre as fases de criação da imagem.
```
## No exemplo, o dockerfile utiliza como base uma imagem Go que tem cerca de 
## 800M (versão 1.16.4). Para facilitar o processo, o stage é nomeado 
## utilizando o argumento `AS`. Ao término do stage, uma nova imageM é 
## utilizada nela, o binário criado no stage anterior, pode ser copiado e 
## utilizado como ENTRYPOINT de uma imagem base mais enxuta como o alpine 
## (cerca de 6M) 
##                                                                               
FROM golang AS imagembase

WORKDIR /app
ADD ./app
run go build -o aplicago

FROM alpine
WORKDIR /app_data
COPY --from=imagembase /app/aplicago /app_data/
ENTRYPOINT ./aplicago
```

### Volumes

Volumes são uma forma de persistir os dados de um container, para que eles fiquem armazenados em definitivo e possam ser utilizados mais de uma vez, já que toda informação de um container é apagada quando ele deixa de existir e nunca é demais ressaltar que containers, devem ser tratados como descartáveis. 

Os volumes podem ser do tipo **bind** ou **volume** e são uma forma fácil de compartilhar arquivos entre containers e hosts e torna-lo mais seguro. Os volumes persistentes também podem ser compartilhados entre eles simultaneamente, mesmo quando um container é excluído. Os volumes do tipo volume, podem ser montados em hosts remotos como em cloud através de drivers.

**Diferenças entre Volume x Bind**

Diferenças|Bind|Volume
----------|----|-------
Comando|`docker container run --mount type=bind,<src>,<dst><image>`|`docker container run --mount type=volume,<src>,<dst><image>`
Características<br/><br/>|- Quando você já tem um diretório local e quer monta-lo no container<br/><br/>- Armazenado diretamente no sistema de arquivos, qualquer processo do sistema operacional pode modificá-lo.<br/><br/>- Não precisa de comandos para cria-lo<br/>|- Armazenado em uma área do sistema de arquivos `/var/lib/docker/volumes`, onde somente o Docker pode controla-lo.<br/><br/>- Necessita que o volume seja criado através do docker `docker volume create`<br/><br/>- Modo preferido de persistir dados
Quando usar|- Compartilhando arquivos de configuração e dados que não são<br/> sensíveis|- Compartilhar arquivos entre containers<br/><br/>- Armazenar arquivos em hosts remotos como cloud
||

**Exemplo de montagem de volumes**
Existem duas formas de montar volumes, a primeira:   

Através do `--mount`, é a maneira mais nova e verbosa. Você utiliza a combinação *chave=valor*, separando os campos de tipo, origem e destino por vírgulas.
```
## Bind
$ docker container run -ti --mount type=bind,src=/opt/bkp/,dst=/bkp/ debian

## Volume, para monta-lo, é preciso antes cria-lo.
$ docker volume create dbdados
$ docker container run -ti --mount type=volume,src=dbdados,dst=/data/ debian
``` 

A segunda e mais antiga `-v` ou `--volume`, combina todas as opções do `--mount`, separando-as por `:`. É possível ainda adicionar o argumento `ro`, onde define o volume como `read only`.

```
## Bind
docker container run -ti -v /opt/bkp/:/bkp/:ro debian

## Volume
docker container run -ti -v dbdados:/data/ debian
```

**Criando um volume**
```
$ docker volume create <nomedovolume>
```

**Listando volumes**
```
$ docker volume ls
```

**Detalhes do volume**
```
$ docker volume inspect <volume>
```

**Remove volumes**
```
$ docker volume rm <volume>

## Remove todos os volumes que não estão associados a um container. 
## Tome cuidado!! Pode conter dados importantes.
$ docker volume prune <volume>
```

>**Container de Backup de volume**
>```
> ## O container monta um volume pré existente, onde os dados estão armazendos 
> ## e monta o diretório onde as informações serão backupeadas.
> ## O container sobe apenas para executar a rotina e depois morre.
> $ docker container run -ti --mount type=volume,src=dbdados,dst=/data --mount type=bind,src=/opt/backup,dst=/backup debian tar -cvf /backup/bkp-banco.tar /data
>```

**Alteração de uma imagem**
É possível a partir de um container em execução, customiza-lo e criar uma imagem a partir dele.
```
$ docker commit <containerid> imagemcustomizada

## Renomeia a imagem customizada
$ docker image tag <id ou nome> nomenovo:1.0
```

### Docker Hub
O [Docker Hub](https://hub.docker.com) é o repositorio oficial (registry) de imagens Docker.

Para enviar uma imagem, é necessário fazer o login primeiro. 

**Login** 
```
$ docker login

$ docker login server.repo.com
```
> Se um server não for setado, é utilizado o repo default do daemon.

**Enviar para o registry**
```
# É necessário renomear primeiro a imagem com o padrao nomedousuario/imagem:versao
$ docker image tag 43254134 scsolutions/apachecustom:1.0

$ docker push scsolutions/apachecustom:1.0
```

**Montar um registry local**   
Para montar um repositório local, basta fazer o seguinte:
```
## 1- Rodar o container Registry
$ docker container run -d -p 5000:5000 --restart=always ==name registry registry:2

## 2- Tagear a imagem para o endereço local
$ docker image tag diogoscs/apachecustom:1.0 localhost:5000/apachecustom:1.0

## 3- Fazer o push
$ docker push localhost:5000/apachecustom:1.0
```
> O comando curl localhost:5000/v2/_catyalog, exibe as imagens do repositorio do Registry


## Docker Machine
Ferramenta que permite criar e gerenciar hosts Docker remotamente em máquinas virtuais e instâncias, localmente ou remotamente. O [Docker Machine](https://docs.docker.com/machine/) utiliza drivers para fazer a ponte com a plataforma como a AWS, Azure, VitualBox, etc.

**Instalação**
É importante consultar a [documentação oficial](https://docs.docker.com/machine/install-machine/) para escolher o método de instalação correto da plataforma. 

**Criar uma VM no VirtualBox**
```
$ docker-machine create --driver virtualbox giropops
```

**Lista hosts Docker**
```
$ docker-machine ls
```

**Exibe o ip do host Docker**
```
$ docker-machine ip 
```

**Exibe as informações detalhadas**
``` 
$ docker-machine inspect vmhost
```

**Se conecta ao host criado**
```
$ docker-machine ssh griopops
```

**Variaveis**
```
## Exibe as variáveis do host e o comando de conexão ao daemon do host.
$ docker-machine env giropops

## Remove a configuração do docker machine
$ docker-machine env -u
```

**Inicia o host**
$ docker-machine start linuxtips

**Encerra o host**
$ docker-machine stop linuxtips

## Docker Swarm e Services
O Swarm é orquestrador do Docker para utliza-lo em cluster. O Manager orquestra, gerencia e delega os workers a executarem os containers, ex, se há um pedido para rodar um container nginx, o manager analisa qual máquina está mais apta a roda-lo. O manager por default também é um worker. É possível executar N managers e N Workers.

<img src="../images/services-diagram.png" alt="VM vs Docker" width="431" height="244">

Podem existir quantos managers desejar, no entanto para que haja alta disponibilidade, você precisa ter sempre 51% dos nodes managers ativos para que o cluster continue funcionando. Em um ambiente com 5 managers, você pode perder até 2, com 4 ou 3 managers, pode perder apenas 1 por exemplo. Quando um manager ativo cai, há uma eleição entre os demais nodes managers, para saber quem será o proximo ativo.
> A comunicação entre os nodes (Workers e Managers) é feita através de algumas portas TCP e UDP,  para saber mais, [consulte a documentação](https://docs.docker.com/engine/swarm/swarm-tutorial/). 

### Swarm

**Iniciando o cluster**
 ```
## O resultado do comando exibirá o id do node locale o comando para adicionar 
## os demais nodes workers a esse manager. 
$ docker swarm init
```
> Você pode anunciar o ip local para o cluster com o parâmetro `--advertise-addr 192.168.0.1`

**Lista os nodes do swarm**
```
$ docker node ls
``` 

**Remover node do swarm**
```
#Sai do modo swarm
$ docker swarm leave

# Remove o node da lista de nodes
$ docer node rm
```
> O argumento `-f` sai forçadamente do swarm, quando um node é um manager

**Adicionando nodes**
``` 
# Para visualizar as instruções e o token para um manager digite
$ docker swarm join-token manager

# Para visualizar as instruções e o token para um worker digite
$ docker swarm join-token worker

# Adicionando o token do worker e do manager, o que muda entre os dois é o token
$ docker swarm join --token <token> <ip:port>
```

**Adicionando nodes**
``` 
# Para visualizar as instruções e o token para um manager digite
$ docker swarm join-token manager

# Para visualizar as instruções e o token para um worker digite
$ docker swarm join-token worker
```
> O argumento `--rotate` atualiza o token. Os nós ativos não caem pois reconhecem a nova chave.

**Promover e despromover um node**
```
# Promover um worker a manager
$ docker node promote <servidor>

# Despromover um manager a worker
$ docker node demote <servidor>
```
> O comando deve ser rodado em um manager.

**Exibe informações do node**
```
$ docker node inspect
```
> O parâmetro `--pretty` coloca os detalhes em modo amigável.

**Atualizando informações do node**   
O subcomando `update` atualiza informações dos nodes dentre eles:
```
# Atualiza o status de disponibilidade do node
$ docker node update --availability drain <node>

# Label no node
$ docker node update --label-add dc=UK <container>
```
> O parâmetro `--availability` atualiza a disponibilidade do node receber containers. A opção `active` diz que o node por aceitar novos containers, a opção `drain` remove os containers do node e os aloca em outros nodes e `pause` não aceita mais containers, mas também não os exclui os existentes. O status atual pode ser visto através do `docker node inspect`.   
> O parâmetro `--label-add` aceita como arugumento valor, ou chave=valor.


### Services

**Criando services**
Para o Swarm e para o Compose, os containers são tratados como services, ele seriam o conjunto de containers com o objetivo de rodar um mesmo serviço ou aplicação. Através do Swarm, é possível rodar containers através do `service create`
```
$ docker service create --name webserver --replicas 3 -p 8080:80 nginx
```
> O argumento `--replicas` é opcional, serve para que o manager possa distribuir os containers entre os nodes e assim criar a alta disponibilidade do serviço.   
> O argumento `--name` dá um nome  (alias) ao serviço.   
> O argumento `-p` binda a porta do serviço no cluster, criando um balanceamento de carga.

**Remover um serviço**
```
$ docker service rm 
```
> Se ao invés do serviço o container for removido com o comando `docker container rm`, o manager subiria novamente o container, pois o serviço ainda está em execução e ele possui as suas réplicas

**Listando serviços**
``` 
# Lista os serviços que rodam no Swarm
$ docker service ls

# Lista o servço, as replicas e onde o service está rodando
$ docker service ps <service>
```

**Exibe informações do serviço**
```
$ docker service inspect
```

**Escala serviço**
```
$ docker service scale <service=N>
```

**Atualização de serviço**
```
$ docker service update

$ docker service update --network-add name=minharede <service>

$ docker service update --replicas=10 <service>
```
> O comando ``docker service update --replicas=<N>`` e ``docker service scale <service=N>`` têm a mesma função de atualizar a quantidade de replicas de um serviço.
> O parâmetro `--network-add` adiciona o service a uma rede.

**Rollback**   
Retorna as configurações do serviço para o estado anterior a última atualização sofrida.
```
# Nesse caso originalmente o serviço foi criado com apenas uma réplica
$ docker service create nginx webserver

# Foi atualizado para utilizar três réplicas
$ docker service update --replicas=3 webserver

# Retornando a configuração anterior com apenas uma réplica
$ docker service rollback <service>
```


### Network

Criar uma rede
```
$ docker network create <rede>
```
> O argumento `-d` determina o tipo de rede a ser criada, `bridge` ou `overlay`. A rede overlay é criada para que o services do swarm possam conversar internamente.

### Secrets
Gerenciar e enviar informações sensíveis que você desejar armazenar em um container no Swarm.
> O secret fica /run/secrets

**Cria secret**
```
# De uma string
echo -n "password" | docker secret create senha -

# De um arquivo
docker secret create <nomesecret> <file>
```

**Lista os secrets**
```
$ docker secret ls
```

**Detalhes da secret**
```
$ docker secret inspect
```

**Remover uma secret**
```
$ docker secret rm
```

**Adicionar secrets**
```
# Adicionar um secret a um sevice na criação
$ docker service create -secret <secret> webserver

# Adicionar uma secret a um service em execução
$ docker service update --secret-add <secret> webserver
```

## Docker Compose e Stack

### Compose
O compose é a ferramenta para rodar aplicações em multi containers. As instruções de build das aplicações, são referenciadas em um arquivo YAML, que tem a função de automatizar a criação de diversos containers simultaneamente, configurando e iniciando os serviços necessários para a montagem do ambiente, chamado de stack. Com o compose você define como você quer que as suas aplicações sejam "deployadas".

**Exemplo de Compose File**
```
version: "3.7" # versão
services: # quais os serviços declarados, nesse caso o serviço se chama web
    web:
      image: nginx # imagem do serviço
      deploy:
        replicas: 5 # quantidade de containers
        resources: # recursos de hw e limites
          limits:
            cpus: "0.1"
            memory: 50M
        restart_policy:
          condition: on-failure # em caso de falha
      ports:
        "8080:80"
      networks:
            webserver # nome da rede q o serviço será deployado
networks: # precisa criar a rede declarada acima
    webserver:
```

### Stack
Conjunto de componentes para que a aplicação funcione, como imagens, aplicações e serviços que compõem o ambiente. Você pode ter um stack/compose para uma aplicação que envia notificações por exemplo, mas nada impede que você construa uma stack para o front end e outra para o backend

**Deploy da stack**
```
## Faz o deploy ou atualiza uma stack de um compose file.
$ docker stack deploy -c docker-stack.yml nomedastack
```

**Listar stacks**
```
# Lista stacks que estão em execução
$ docker stack ls

# Exibe onde estão rodando os containers da stack
$ docker stack ps <stack>

# Exibe os serviços da stack
$ docker stack services
```

**Remove a stack**
```
$ docker stack rm
```

## Laboratorios UNDER CONSTRUCTION
Exercícios para fixar o conteúdo    

**Lab 1 - Informações do container**
?(subir um container e atualizar informações e stressa-lo)?
?(Montar volumes: bind (compartilhar entre containers) 
?(customizar a imagem e criar uma nova utilizando o docker commit)?

**Lab 2**   
crirar duas vms, comaprtilhar um volume/volume atraves do volumes from (data only), remover os container e remontar o volume em dois containers (da forma nova) para ver se o dado está la)?

**Lab3**   
- subir o docker file (baixar aqui, pasta lab3), testando a imagem criada (webserver), observar a opção de volume q foi passada dockerfile
- fazer o envio da imagem para o docker hub

**Lab 4**
-Aplicação go/python (posso fazer depois)

**Lab 5**
- Subir uma docker machine na AWS

**Lab6**
- montar um cluster com services e montar volume
- montar um cluster swarm com 3 nodes
- escalar containers em um serviço
- Testar a remoção de um container com o docker container rm
- Colcar em varias redes
- ver docker network inspect   

### Laboratório 7

**Pré requisito:**   
 Docker Swarm com 3 nodes.

**Instruções**   
- Fazer o deploy do [docker-compose-lab7](files/lab7/docker-compose-lab7.yml) 

> Resultado esperado: Cinco réplicas do nginx latest
- O stack precisa ter 5 réplicas
- Testar a rede
- Testar o serviço web
- Remover o stack ao final

## Laboratório 8

**Pré requisito:**   
Docker Swarm com 2 nodes.

**Instruções**   
- Fazer o deploy do [docker-compose-lab8](files/lab8/docker-compose-lab8.yml) 

> Resultado esperado: Dois serviços rodando, Wordpress e um Mysql (um em cada node). O banco de dados deve ter um volume chamado db_data.
- Verificar se os serviços foram criados
- Testar o Wordpress e se logar pela primeira vez
- Verificar a criação do volume
- Remover o stack ao final

## Laboratório 9

**Pré requisito:** 
Docker Swarm com 3 nodes. Mudar o label de um dos nodes, para o que está referenciado no compose.

**Instruções**   
1. Fazer o deploy do [docker-compose-lab9](files/lab9/docker-compose-lab9.yml) 
2. No Visualizer, verificar a composição das cinco réplicas
3. Alterar o label do node que está sem conainers para o label idêntico do compose, para que haja balanceamento
4. Fazer um scale para 10 réplicas
  
> Resultado esperado: Dez réplicas do nginx latest. Uma réplica do Visualizer.
- O Visualizer precisa estar rodando em um manager
- Verificar a existência de dez replicas nos nodes, de maneira balanceada
- Testar o serviço web
- Remover o stack ao final

  










**Lab10**


- Compose4

**Lab11**
- Compose5

**Lab12**
-Criar uma secret e enviar para um container e depois atualizar removendo e enviado outra (pra varios containers em escala). O segundo secret tem q ter permissão 0400 para root

**Lab13**
- montar um volume na aws (s3)

**Lab14**
- criar um pipeline para rodar o docker automaticamente na AWS