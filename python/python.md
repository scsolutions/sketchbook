# Python

## Strings

### Métodos para confirmar atributos
Todo objeto (variavel) possui caracteristicas e funcionalidades, é possível utilizar então métodos para ler atributos de uma string. Vale ressaltar que nos exemplos a seguir, apenas o método `type()`, serve para todos os tipos primitivos, os demais, são exclusivos para o tipo *string*, para mais informações, consulte a (documentação oficial)[https://docs.python.org/3/contents.html] ou a [documentação de referência](https://docs.python.org/3/library/stdtypes.html#str).

> Nos exemplos abaixo vamos considerar a variável `palavra = 'Colega'`.

Tipo primitivo de um valor ``type(var)``.
```
$ type(palavra)
<class 'str'>
```
> Alem de string, os tipos primitivos podem se inteiros (integer), booleanos ou flutuantes (float).    

O metodo ``str.isalpha`` retorna **True** se houver ao menos um caracter e todos eles forem letras.
```
$ palavra.isalpha
True
```

O metodo ``str.isnumeric`` retorna **True** se houver ao menos um caracter e todos eles forem numéricos
```
$ palavra.isnumeric
False
```

O metodo ``str.isalnum`` retorna **True** se houver ao menos um caracter e todos eles forem alfanuméricos.
```
$ palavra.isalnum
True
```

O metodo ``str.isascii`` retorna **True** se, a string for vazia ou se todos os caracteres na string forem ASCII.
```
$ palavra.isascii
True
```

O metodo ``str.isdecimal`` retorna **True** se, todos os caracateres forem decimais e a string nao estover vazia.
```
$ palavra.isdecimal
False
```

O metodo ``str.isidentifier`` retorna **True** se a string for um identificador valido. Ver [Identificadores e Keywords](https://www.programiz.com/python-programming/keywords-identifier).
```
$ palavra.isidentifier
True
```

CONTINUAR = PARADO EM 04/07 = PARADO NO DESAFIO 04


print()

print('--- Metodo str.isdigit ---')
print('Resultado >>>', n.isdigit())
print(f'Explicacao: O metodo "str.isdigit" retorna True se, todos os\n'
      f'caracteres forem digitos e existir ao menos um.')

print()

print('--- Metodo str.islower ---')
print('Resultado >>>', n.islower())
print(f'Explicacao: O metodo "str.islower" retorna True se, todos os caracteres\n'
      f' em caixa forem minusculos e existir ao menos um.')

print()

print('--- Metodo str.isupper ---')
print('Resultado >>>', n.isupper())
print(f'Explicacao: O metodo "str.isupper" retorna True se, todos os caracteres\n'
      f' estao em caixa alta e existir ao menos um.')

print()

print('--- Metodo isprintable ---')
print('Resultado >>>', n.isprintable())
print(f'Explicacao: O metodo "str.isprintable" retorna True se, a string puder ser\n'
      f'impressa ou vazia.')

print()

print('--- Metodo isspace ---')
print('Resultado >>>', n.isspace())
print(f'Explicacao: O metodo "str.isspace" retorna True se, existirem apenas\n'
      f'caracteres em branco e existe pelo menos um caracter.')

print()

print('--- Metodo istitle ---')
print('Resultado >>>', n.istitle())
print(f'Explicacao: O metodo "str.istitle" retorna True se, a string for\n'
      f' titlecased (capitalizada) e existe ao menos um caracter')


## Conjuntos

O **cojunto/set** em Python utiliza o simbolo de "chaves" `{}` para representa-lo. Um conjunto pode ser exibido da seguinte maneira:
```
$ conjunto = {1, 2, 3, 4}
$ print(conjunto)
{1, 2, 3, 4}
```

Não podem existir duplicidades em conjuntos, além disso, os elementos **numerais** sempre aparecem ordenados, por exemplo: 
```
$ conjunto = {1, 2, 4, 4, 3}
$ print(conjunto)
{1, 2, 3, 4}
```

Adicionar e remover um elemento ao conjunto:
```
$ conjunto = {1, 2, 3, 4, 4}
$ conjunto.add(5)
$ print(conjunto)
{1, 2, 3, 4, 5}

$ conjunto.discard(4)
$ print(conjunto)
{1, 2, 3, 5}

# Para remover, é possível utilizar também o remove
$ conjunto.remove(3)
{1, 2, 5}
```

Unir dois conjuntos:
```
$ conjunto_a = {1, 3, 5}
$ conjunto_b = {2, 4, 6}
$ conjunto_a.union(conjunto_b)
{1, 2, 3, 4, 5, 6}
```

Interceção de conjuntos:
```
$ conjunto_a = {1, 2, 5}
$ conjunto_b = {1, 2, 3, 4, 5, 6}
$ conjunto_a.intersection(conjunto_b)
{1, 2, 5}
```

Diferença de elementos entre conjuntos: 
```
$ conjunto_a = {1, 2, 5}
$ conjunto_b = {8, 5, 6}
$ conjunto_a.difference(conjunto_b)
{1, 2}

# A ordem das variáveis, influencia no resultado.
$ conjunto_b.difference(conjunto_a)
{6, 8}

# Ao contrario da intercessão, a symmetric_difference, exibe a diferença entre 
# todos os conjuntos
$ conjunto_a.symmetric.difference(conjunto_b)
{1, 2, 6, 8}
```

Subconjunto e Superconjunto
> Se o conjunto *X* for subconjunto do conjunto *Y*, o *Y* sempre será super conjunto de *X* e vice e versa.
```
$ conjunto_a = {1, 2, 3, 4, 5, 6}
$ conjunto_b = {1, 2, 5}

# Quando um conjunto está contido em outro, ele é um subconjunto e retorna um 
# True se essa condição for verdadeira e False se não for.
$ conjunto_b.issubset(conjunto_a)
True 

$ conjunto_a.issubset(conjunto_b)
False

# Quando um conjunto contém outro, ele é um superconjunto e retorna um True se
# essa condição for verdadeira e False se não for.
$ conjunto_a.issuperset(conjunto_b)
True 

$ conjunto_b.issuperset(conjunto_a)
False
```
    
> Dica: Para converter uma lista em conjunto, e assim ordenar os elementos (numeros) e remover suas duplicidades, converta-a em um conjunto.
> ```
> $ lista = [2, 3, 3, 1]
> $ set(lista)
> {1, 2, 3}
> ```

## Metodos e Funções
Metodo (não retorna valor) x Função (tudo aquilo q retorna valor)

declarar metodo ou função, utiliza-se "def", ex:
def parabens():
Uma função fora de uma classe se chama função, dentro, se chama classe

Declarar classse
class Calculadora:

Classe começam com letra maiuscula ou _, Função e metodos com letras minusculas

A classe agrupa os metodos em um objetivo comum

Bibliotecas --> módulo --> funções

funções possuem atributos, em datetime.date.today().day o atributo é .day, ele traz a informação do dia atual

funções são códigos prontos

## Laboratórios

**Laboratório 1: xxx**   

**Laboratório 2: Respondendo ao usuário**   
Faça um programa que leia o nome de uma pessoa e mostre uma mensagem de boas-vindas. Fonte [aqui](https://youtu.be/FNqdV5Zb_5Q).
> Objetivo: utilização do ``input`` para ler um dado do usuário.

**Laboratório 3: Somando dois números**   
Crie um programa que leia dois números e mostre a soma entre eles. Inspirado originalmente [aqui](https://youtu.be/PB254Cfjlyk)
> Operações aritiméticas simples.

**Laboratório 4: Dissecando uma Variável**   
Faça um programa que leia algo pelo teclado e mostre na tela o seu tipo primitivo e todas as informações possíveis sobre ele. Inspirado originalmente [aqui](https://youtu.be/tHYxjJxtJko)
> Usar métodos para identificar os atributos de uma string [Documentação auxiliar](https://docs.python.org/3/library/stdtypes.html#str).
Exemplo de output do programa:
```
$ python laboratorio4.py

Digite algo: Casinha

O tipo primitivo desse valor é <class 'str'>
Só tem espaços? False
É um número? False
É alfabético? True
É alfanumerico? True
Está em maiusculas? False
Está em minusculas? False
Está capitalizada? True
```