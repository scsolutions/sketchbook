
#### Lógica de Provisionamento de infraestrutura

1- O binário lê o arquivo de configuração.
2- O arquivo binário compara com o state.
3- Executa as modificações e inclusões, mediante as informações encontradas no arquivo (ou plan) em comparação com o estado.
4- Provisiona os recursos, baseado no estado.


**Provider** define o provedor de integração. Cada provedor possui configurações específicas, como AWS, Google, etc. Todo provider necessita de credenciais para interagir com o provedor específico.
```
provider "aws" {
    region = us-east-1
}
```
### Comandos
**Init**
Primeiro comando de execução do Terraform, configura a pasta de configuração inicial do Terraform, baseado nas configurações do provider, baixando plugins e requisitos necessários para a execução.
```
$ terraform init
```

**Plan**
O comando ``terraform plan`` gera um plano de provisionamento dos recursos.
```
$ terraform plan

# Gera o plano em um arquivo.
$ terraform plan -out plano
```

**Apply**
O comando ``terraform apply``, cria os recursos no provider, aplicando o plano gerado pelo `plan`, ao mesmo tempo em que gera o estado (state).
```
$ terraform apply

# Aplica um plano a partir de um arquivo.
$ terraform apply plano
```
Fluxo inicial do Terraform para gerar o estado.
> `$ terra form plan -> PLANO -> $ terraform apply -> ESTADO -> RECURSOS`

**Destroy**
O comando ``terraform destroy``, apaga o que existe no provider, baseado no que está no estado. O recurso é destruído normalmente quando o provider, entende que o recurso precisa ser destruído e recriado. 
```
$ terraform destroy
```

> Os **argumentos** são as informações fornecidas no provisionamento e criação da infraestrutura. Os **atributos** são as informações de saída, passados após o provisionamento.

**State**
O state armazena o estado da infraestrutura gerenciada, utilizado para mapear os recursos existentes no ambiente. Ele pode ser armazenado remotamente, no entanto, por default ele se encontra no arquivo json terraform.tfstate.

```
# Faz o plan, dando um bypass no refresh do state
$ terraform plan refresh=false

# Atualiza o state
$ terraform refresh
```

É possível baixar e subir para o backend  o estado atual do ambiente com os sub-comandos `pull` que pode também ser direcionado a saída para um arquivo e o para atualizar `push` a infraestrutura.

```
$ terraform state pull

$ terraform state pull >> estadoatual.tfstate

$ terraform state push estadoatual.tfstate
```
> Sempre que o state for atualizado manualmente através do push, é necessário atualizar a chave *serial* para um valor maior.

O *State Locking* serve para que o backend "trave" o state, o objetivo é de que um usuário não consiga executar modificações no estado atual, enquanto estiver ocorrendo uma operação. Na AWS, é necessário criar uma tabela no DynamoDB e aponta-la no backend.

O sub-comando ``import``, importa as configurações atuais da infraestutura, diratamente para que o state assimile o ambiente.
> Para importa-lo, é necessário cria-lo no modulo root.
```
$ terraform import aws_instance.example i-12345
```
> Quando a importação é completa, todos os dados vinculados, também são trazidos, ex: SG e suas regras.

**Console**
O ``terraform console``, é o console do Terraform para validar e experimentar expressões interagindo por padrão com o estado. Com ele é possível também consultar informações do provider, através do recurso do tipo *data*.

> Para efetuar uma consulta em um recurso do tipo data, é necessário incialmente "declara-lo".
```
$ terraform console

$ data.aws_ec2_instance_type.instance_type_custom.memory_size
```
> Os arquivos *terraform.tfvars* e *terraform.tfvars.json* são utilizado por padrão, para declarar variáveis para o Terraform e não precisam ser referenciados para serem utilizados.

**Workspace**
Por padrão, o Terraform trabalha com um workspace (default), no entanto, alguns backends suportam múltiplos workspaces, que são as instâncias de configuração a serem deployadas (modelos de estado), utilizando o mesmo backend. 

```
# Cria um novo workspace
$ terraform workspace new "web"

# Exibe a lista de workspace existentes
$ terraform workspace list

# Seleciona um workspace
$ terraform workspace select prd
```

#### Provisioners
São usados para executar ações de gerenciamento de configuração em máquinas locais ou remotas, para preparar as infraestrutura para os serviços após a sua aplicação. São similares a serviço executado por Ansible e Puppet.
Por default os provisioners são executados somente na criação do recursos.
Os provisioner são vinculados diretamente nos recursos, ex:
```
# O comando abaixo é executado na máquina local
resource "aws_instance" "web" {
  # ...

  provisioner "local-exec" {
    command = "echo The server's IP address is ${self.private_ip}"
  }
}
```
> **DICA:** No provisioner, devido a dependências, só é possível fazer referências a objetos externos como atributos e argumentos, através do ``self`` .

O *when* é um atributo que altera o comportamento padrão do provisioner, ao invés de executar na criação, ele pode ser alterado para rodar ao ser destruído.

O atributo *on-failure*, configura comportamentos para a falha, por padrão o provisioner interrompe o provisionamento da infraestrutura se houver um erro.

É possível usar o ``local-exec``, executando comandos locais, onde o Terraform está alocado.

O ``remote-exec`` executa um comando na máquina destino somente através de *SSH* ou *winrm*.
````
resource "aws_instance" "web" {

  # Establishes connection to be used by all
  # generic remote provisioners (i.e. file/remote-exec)
  connection {
    type     = "ssh"
    user     = "root"
    password = var.root_password
    host     = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "puppet apply",
      "consul join ${aws_instance.web.private_ip}",
    ]
  }
}

````

> **DICA:** Se um provisioner estiver marcado como taint, ele não será executado para destruir, mesmo que esteja marcado para tal.




EXERCICIO: CRIAR UMA INSTANCIA ATRAVÉS DO TERRAFORM E INSTALAR NGINX ATRAVÉS DO REMOTE-EXEC, utilizar repositorio local e depois na AWS
TESTAR CONSOLE E DATA. na saida do comando, cuspir o ip e o dns publico, usar alias também (multi region)

Importar um recurso pré-existente, Criar dois modulos, simular um state locking, simular uma atualização do refresh. Criar um workspace customizado em duas regioes diferentes e nomes de recursos tambem

