# Mysql

## Instalação

### Ubuntu 20.04
*(verificado em 07/2021)*

1. Atualiza repositório
```
$ sudo apt-get update
```

2. Instala o pacote do MariaDB
```
$ sudo apt-get install mariadb-server

```

2. Checar se o serviço está rodando e habilitado
```
$ sudo systemctl status mariadb
```

3. Em seguida, rodar o script de segurança.
```
$ sudo mysql_secure_installation
```

4. Nas perguntas as seguir:
1. Responder nao para a senha
2. Setar a nova senha
3. Remover usuario anonimo e as demais utiizar a opção default

5. Criar um usuário com privilegios administrativos, para nao usar o root
```
$ mysql -u root -p 
> create user 'admin'@'localhost' identified by 'ibeu1234'

> grant all on *.* to 'admin'@'localhost' identified by 'ibeu1234' with grant option;
    
> flush privileges; 

$ mysql -u admin -p 

```

> Baseado em [fatos reais](https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-ubuntu-20-04-pt).   

## Criar banco]

```
> create database <banco>; 

> use <banco>
```

